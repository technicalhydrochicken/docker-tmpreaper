#!/usr/bin/env sh

set -x
set -e

VERSION_TAG=latest



if [[ -z "$CI_REGISTRY_IMAGE" ]]; then
    docker build . --build-arg VERSION=${VERSION_TAG}
else
    # Change this to something more generic when possible
    IMAGE_NAME=$CI_REGISTRY_IMAGE

    docker build . -t "$IMAGE_NAME:${VERSION_TAG}"
    docker push "$IMAGE_NAME:latest"
fi
