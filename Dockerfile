FROM ubuntu:18.04
RUN apt-get -y update && \
    apt-get -y install --no-install-recommends tmpreaper && \
    rm -rf /var/lib/apt/lists/*

VOLUME /data

ENTRYPOINT ["tmpreaper"]
